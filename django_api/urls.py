from django.contrib import admin
from django.urls import path, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api-auth/', include('rest_framework.urls')),  # Para autenticación basada en sesión
    path('api/users/', include('users.urls')),
    path('api/inventory/', include('inventory.urls')),
    path('api/production-line/', include('production_line.urls')),
    path('api/sales/', include('sales.urls')),
    path('api/tickets/', include('tickets.urls')),
    path('api/delivery/', include('delivery.urls')),
]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
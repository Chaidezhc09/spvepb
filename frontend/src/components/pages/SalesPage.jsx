import React, { useEffect, useState } from 'react';
import { Navbar, Container, Row, Col, Card, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './SalesPage.css'; 
import { apiClient } from '../../ApiClient';

const SalesPage = () => {
    const [products, setProducts] = useState([]);

    useEffect(() => {
        const fetchProducts = async () => {
            try {
                const response = await apiClient.get('/inventory/products/');
                setProducts(response.data);
                console.log(response.data);
            } catch (error) {
                console.error('Error fetching products:', error);
            }
        };

        fetchProducts();
    }, []);

    return (
        <div>
            <Container className="container-custom">
                <h1>Store</h1>
                <Row>
                    {products.map(product => (
                        <Col key={product.product_id} xs={12} sm={6} md={4} lg={3}>
                            <Card className="card-custom">
                                <div className="card-img-container">
                                    <Card.Img src={product.image_icon} className="card-img-top" />
                                </div>
                                <Card.Body className="card-body-custom">
                                    <Card.Title className="card-title-custom" title={product.name}>{product.name}</Card.Title>
                                    <Card.Text className="card-text-custom">
                                        <strong>${product.price}</strong>
                                    </Card.Text>
                                    <Button className="button-custom">ADD TO CART</Button>
                                </Card.Body>
                            </Card>
                        </Col>
                    ))}
                </Row>
            </Container>
        </div>
    );
};

export default SalesPage;
